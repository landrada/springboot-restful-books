# Specifications
* Spring boot app initializer
* Spring security
* Spring JPA
* Spring MVC
* Swagger
* jUnits
* mockito
* API Requests Validatios